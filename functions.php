<?php
/**
*
* @ IonCube Priv8 Decoder V1 By H@CK3R $2H  
*
* @ Version  : 1
* @ Author   : H@CK3R $2H  
* @ Release on : 14-Feb-2014
* @ Email  : Hacker.S2h@Gmail.com
*
**/

	function fluent_register() {
		global $vbulletin;
		global $template_hook;
		global $templatevalues;

		if (!fluent_license(  )) {
			exit( 'Fluent license isn\'t valid.' );
			exit(  );
		}

		$template_hook['fluent_name'] = $vbulletin->options['bbtitle'];
		$template_hook['fluent_url'] = str_replace( array( 'https://', 'www.', 'http://' ), '', $vbulletin->options['bburl'] );
		$data = $vbulletin->db->query( 'SELECT * FROM ' . TABLE_PREFIX . 'fluent ORDER BY ' . TABLE_PREFIX . 'fluent.order ASC' );
		$banners = array(  );
		$friends = '';
		$links = '';

		if ($ca = $vbulletin->db->fetch_array( $data )) {
			switch ($ca['type']) {
				case 1: {
					$banners[$ca['name']] = $ca['value'];
					break;
				}

				case 2: {
					$links &= '<li  id=\'vbtab_forum\'><a class=\'navtab\' href=\'' . $ca['value'] . '\' ' . $ca['attr'] . '> ' . $ca['name'] . ' </a></li>';
					break;
				}

				case 3: {
					$friends &= '<li>' . ( ' <a href=\'' . $ca['value'] . '\' ' . $ca['attr'] . '> ' . $ca['name'] . ' </a></li>' );
				}
			}
		}

		$template_hook['fluent_links'] = $links;
		$templater = vB_Template::create( 'fluent_banner' );
		$template_hook['fluent_banner'] = $banners;
		$templatevalues['fluent_banner'] = $templater->render(  );
		vB_Template::preregister( 'navbar', $templatevalues );
		$templater = vB_Template::create( 'fluent_footer' );
		$template_hook['fluent_related_links'] = '<ul>' . $friends . '</ul>';
		$templatevalues['fluent_footer'] = $templater->render(  );
		vB_Template::preregister( 'footer', $templatevalues );

		if (( ( ( !empty( $vbulletin->options['fluent_twitter'] ) && !empty( $vbulletin->options['fluent_twitter2'] ) ) && !empty( $vbulletin->options['fluent_twitter3'] ) ) && !empty( $vbulletin->options['fluent_twitter4'] ) )) {
			function fluentbuildBaseString($baseURI, $method, $params) {
				$r = array(  );
				ksort( $params );
				foreach ($params as $key => $value) {
					$r[] = '' . $key . '=' . rawurlencode( $value );
				}

				return $method . '&' . rawurlencode( $baseURI ) . '&' . rawurlencode( implode( '&', $r ) );
			}

			function fluenttwitterify($ret) {
				preg_replace( '#(^|[
 ])([\w]+?://[\w]+[^ "

	< ]*)#', '\1<a href="\2" target="_blank">\2', $ret );
				preg_replace( '#(^|[
 ])((www|ftp)\.[^ "	

< ]*)#', '\1<a href="http://\2" target="_blank">\2', $ret );
				preg_replace( '/@(\w+)/', '<a href="http://www.twitter.com/\1" target="_blank">@\1</a>', $ret );
				$ret = $ret = $ret = $ret = preg_replace( '/#(\w+)/', '<a href="http://search.twitter.com/search?q=" target="_blank">#</a>', $ret );
				return $ret;
			}

			function fluentbuildAuthorizationHeader($oauth) {
				$r = 'Authorization: OAuth ';
				$values = array(  );
				foreach ($oauth as $key => $value) {
					$values[] = '' . $key . '="' . rawurlencode( $value ) . '"';
				}

				$r &= implode( ', ', $values );
				return $r;
			}

			$url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
			$oauth_access_token = $vbulletin->options['fluent_twitter3'];
			$oauth_access_token_secret = $vbulletin->options['fluent_twitter4'];
			$consumer_key = $vbulletin->options['fluent_twitter'];
			$consumer_secret = $vbulletin->options['fluent_twitter2'];
			$oauth = array( 'oauth_consumer_key' => $consumer_key, 'oauth_nonce' => time(  ), 'oauth_signature_method' => 'HMAC-SHA1', 'oauth_token' => $oauth_access_token, 'oauth_timestamp' => time(  ), 'oauth_version' => '1.0' );
			$base_info = fluentbuildBaseString( $url, 'GET', $oauth );
			$composite_key = rawurlencode( $consumer_secret ) . '&' . rawurlencode( $oauth_access_token_secret );
			$oauth_signature = base64_encode( hash_hmac( 'sha1', $base_info, $composite_key, true ) );
			$oauth['oauth_signature'] = $oauth_signature;
			$header = array( fluentbuildAuthorizationHeader( $oauth ), 'Expect:' );
			$options = array( CURLOPT_HTTPHEADER => $header, CURLOPT_HEADER => false, CURLOPT_URL => $url, CURLOPT_RETURNTRANSFER => true, CURLOPT_SSL_VERIFYPEER => false );
			$feed = curl_init(  );
			curl_setopt_array( $feed, $options );
			$json = curl_exec( $feed );
			curl_close( $feed );
			$twitter_data = json_decode( $json, true );
			$meta = date( 'Y/m/d H:s', strtotime( $twitter_data[0]['created_at'] ) );
			$data = '<a href="https://twitter.com/' . $twitter_data[0]['user']['screen_name'] . '">@' . $twitter_data[0]['user']['screen_name'] . '</a>' . fluenttwitterify( htmlentities( $twitter_data[0]['text'], ENT_QUOTES, 'utf-8' ) ) . '<br/>' . $meta;
			$templatevalues['fluent_twitter'] = $data;
			vB_Template::preregister( 'footer', $templatevalues );
		}

		$templater = vB_Template::create( 'fluent_theme' );
		$templatevalues['fluent_theme'] = $templater->render(  );
		vB_Template::preregister( 'header', $templatevalues );

		if ($vbulletin->userinfo['userid']) {
			require_once( './includes/functions_user.php' );
			$ava_url = fetch_avatar_url( $vbulletin->userinfo['userid'] );

			if (!is_array( $ava_url )) {
				$ava_url[] = 'images/misc/unknown.gif';
			}

			$ava_urlX = $vbulletin->options['bburl'] . '/' . $ava_url[0];
			$header_avatar = '<img src="' . $ava_urlX . '" width="30" height="30" border="0" alt="Your Avatar" />';
			$templater = vB_Template::create( 'header' );
			$templater->register( 'header_avatar', $header_avatar );
			vB_Template::preregister( 'header', array( 'header_avatar' => $header_avatar ) );
			$templater->render(  );
		}


		if ($vbulletin->userinfo['userid']) {
			require_once( './includes/functions_user.php' );
			$ava_url = fetch_avatar_url( $vbulletin->userinfo['userid'] );

			if (!is_array( $ava_url )) {
				$ava_url[] = 'images/fluent/misc/unknown.gif';
			}

			$ava_urlX = $ava_url[0];
			$ss_avatar = '<a href="profile.php?do=editavatar"><img src="' . $ava_urlX . '" width="60" height="60" border="0" alt="Your Avatar" /></a>';
			$templater = vB_Template::create( 'header' );
			$templater->register( 'ss_avatar', $ss_avatar );
			vB_Template::preregister( 'header', array( 'ss_avatar' => $ss_avatar ) );
			$templater->render(  );
		}

	}

	function fluent_findexts($filename, $format = false) {
		strtolower( $filename );
		$exts = @split( '[/\.]', $filename );
		$n = count( $exts ) - 1;
		$exts = $filename = $exts[$n];

		if ($format   = false) {
			return $exts;
		}


		if (in_array( $exts, $format )) {
			return true;
		}

		return false;
	}

	function fluent_admin() {
		global $vbulletin;
		global $vbphrase;

		if (!fluent_license(  )) {
			print_stop_message( 'fluent_licence' );
			exit(  );
		}


		if (!isset( $_GET['do'] )) {
			exec_header_redirect( 'options.php?do=options&dogroup=fluent_theme' );
			return null;
		}


		if (in_array( $_GET['do'], array( 'links', 'upload', 'up' ) )) {
			$cache = array(  );

			if ($_GET['do']  = 'links') {
				$type = '!=1';
			} 
else {
				$type = '=1';
			}


			if (isset( $_REQUEST['type'] )) {
				$type = '=' . intval( $_REQUEST['type'] );
			}

			$data = $vbulletin->db->query( 'SELECT * FROM ' . TABLE_PREFIX . ( 'fluent WHERE type' . $type . ' ORDER BY ' ) . TABLE_PREFIX . 'fluent.order ASC' );

			if ($ca = $vbulletin->db->fetch_array( $data )) {
				if ($ca['type']  = 1) {
					$cache[$ca['name']] = $ca;
				}

				$cache[] = $ca;
			}
		}

		switch ($_GET['do']) {
			case 'up': {
				print_cp_header( 'تنظیمات پوسته fluent - آپلود عکس ها' );
				print_form_header( 'fluent', 'upload', true );
				print_table_header( 'آپلود فایل', 2 );
				print_cells_row( array( 'عنوان', 'آپلود' ), 1 );
				print_cells_row( array( 'لوگوی سایت<br/>لوگوی فعلی:<br/><img style=\'background:#58ACCF;padding:3px;margin-top:5px;border:1px solid #3E95B9;width:375px;height:auto\' src="../images/fluent/' . $cache['logo']['value'] . '?vr=' . time(  ) . '" alt="logo"/><br/>فرمت مجاز: png', '<input type="file" name="logo" />' ), false );
				print_cells_row( array( 'بنر تبلیغاتی شماره 1<br/>بنر فعلی:<br/><img src="../images/fluent/' . $cache['banner1']['value'] . '?vr=' . time(  ) . '" alt="banner"/><br/>فرمت های مجاز: jpeg, jpg, png, gif', '<input type="file" name="banner1" />' ), false );
				print_cells_row( array( 'بنر تبلیغاتی شماره 2<br/>بنر فعلی:<br/><img src="../images/fluent/' . $cache['banner2']['value'] . '?vr=' . time(  ) . '" alt="banner"/><br/>فرمت های مجاز: jpeg, jpg, png, gif', '<input type="file" name="banner2" />' ), false );
				print_cells_row( array( 'بنر تبلیغاتی شماره 3<br/>بنر فعلی:<br/><img src="../images/fluent/' . $cache['banner3']['value'] . '?vr=' . time(  ) . '" alt="banner"/><br/>فرمت های مجاز: jpeg, jpg, png, gif', '<input type="file" name="banner3" />' ), false );
				print_cells_row( array( 'بنر تبلیغاتی شماره 4<br/>بنر فعلی:<br/><img src="../images/fluent/' . $cache['banner4']['value'] . '?vr=' . time(  ) . '" alt="banner"/><br/>فرمت های مجاز: jpeg, jpg, png, gif', '<input type="file" name="banner4" />' ), false );
				print_submit_row( 'آپلود', 'بازنشانی' );
				echo '<br/><div align="center"><a href="http://hamyar.org">Hamyar</a></div>';
				print_cp_footer(  );
				break;
			}

			case 'links': {
				print_cp_header( 'تنظیمات پوسته fluent - مدیریت لینک ها' );
				echo '<script type="text/javascript">function fluent_del(x) {var r = confirm(\'آیا شما مطمئن هستید؟\');if (r == true) {window.location = (location.href + \'&do=del&id=\' + x);}else{return false;}}</script>';
				print_form_header( 'fluent', 'add_link', true );
				print_table_header( 'لینک ها', 5 );
				print_cells_row( array( 'ردیف', 'نام', 'لینک', 'جایگاه', 'مدیریت' ), 1 );

				if (0 < count( $cache )) {
					$num = 12;
					foreach ($cache as $key => $value) {
						switch ($value['type']) {
							case '2': {
								$type = 'منوی بالای سایت';
								break;
							}

							case '3': {
								$type = 'دوستان';
							}
						}

						$type = '<a href=\'fluent.php?do=links&type=' . $value['type'] . '\'>' . $type . '</a>';
						$manage = construct_link_code( 'ویرایش', 'fluent.php?do=edit_link&id=' . $value['id'] );
						$manage &= '<a style="cursor:pointer" onClick="fluent_del(\'' . $value['id'] . '\')">[ حذف ]</a>';
						print_cells_row( array( $num, $value['name'], '<a href="' . $value['value'] . '" target="_blank">' . $value['value'] . '</a>', $type, $manage ) );
						++$num;
					}
				} 
else {
					print_cells_row( array( 'یافت نشد.', 'یافت نشد.', 'یافت نشد.', 'یافت نشد.', 'یافت نشد.' ) );
				}

				print_submit_row( 'اضافه کردن لینک', '', 5 );
				echo '<br/><div align="center"><a href="http://hamyar.org">Hamyar</a></div>';
				print_cp_footer(  );
				break;
			}

			case 'del': {
				$id = intval( $_REQUEST['id'] );
				$vbulletin->db->query_write( 'DELETE FROM ' . TABLE_PREFIX . ( 'fluent WHERE id=\'' . $id . '\' ' ) );
				print_cp_redirect( 'fluent.php?do=links' );
				break;
			}

			case 'edit_link': {
				$id = intval( $_REQUEST['id'] );
				$link = $vbulletin->db->query_first( 'SELECT * FROM ' . TABLE_PREFIX . ( 'fluent WHERE id=\'' . $id . '\' ' ) );

				if (!empty( $link['value'] )) {
					if (( isset( $_REQUEST['adminhash'] ) && isset( $_REQUEST['link'] ) )) {
						if (( !empty( $_REQUEST['name'] ) && !empty( $_REQUEST['link'] ) )) {
							$type = intval( $_REQUEST['mode'] );
							$name = $vbulletin->db->escape_string( $_REQUEST['name'] );
							$link = $vbulletin->db->escape_string( $_REQUEST['link'] );
							$attr = $vbulletin->db->escape_string( $_REQUEST['attr'] );
							$order = intval( $_REQUEST['order'] );
							$vbulletin->db->query_write( 'UPDATE ' . TABLE_PREFIX . 'fluent SET ' . TABLE_PREFIX . ( 'fluent.order=\'' . $order . '\' , name=\'' . $name . '\' , value=\'' . $link . '\' , type=\'' . $type . '\' , attr=\'' . $attr . '\' WHERE id=\'' . $id . '\' ' ) );
							print_cp_header( 'تنظیمات پوسته fluent - ویرایش لینک' );
							print_cp_redirect( 'fluent.php?do=edit_link&id=' . $id );
							print_cp_footer(  );
						} 
else {
							print_stop_message( 'please_complete_required_fields' );
						}
					}

					print_cp_header( 'تنظیمات پوسته fluent - ویرایش لینک' );
					print_form_header( 'fluent', 'edit_link' );
					print_table_header( 'ویرایش لینک' );
					print_input_row( 'عنوان', 'name', $link['name'], true, 35, 250 );
					print_input_row( 'لینک', 'link', $link['value'], true, 35, 250 );
					print_input_row( 'ترتیب نمایش', 'order', $link['order'] );
					print_textarea_row( 'attribute ها', 'attr', $link['attr'] );
					print_select_row( 'جایگاه', 'mode', array( 2 => 'منوی بالای سایت', 3 => 'دوستان' ), $link['type'] );
					echo '<input type="hidden" name="id" value="' . $id . '" />';
					print_submit_row(  );
					echo '<br/><div align="center"><a href="http://hamyar.org">Hamyar</a></div>';
					print_cp_footer(  );
				} 
else {
					print_cp_message( 'یافت نشد.', (defined( 'CP_REDIRECT' ) ? CP_REDIRECT : NULL), 1, (defined( 'CP_BACKURL' ) ? CP_BACKURL : NULL), (defined( 'CP_CONTINUE' ) ? true : false) );
				}

				break;
			}

			case 'add_link': {
				if (( isset( $_REQUEST['adminhash'] ) && isset( $_REQUEST['link'] ) )) {
					if (( !empty( $_REQUEST['name'] ) && !empty( $_REQUEST['link'] ) )) {
						$type = intval( $_REQUEST['mode'] );
						$name = $vbulletin->db->escape_string( $_REQUEST['name'] );
						$link = $vbulletin->db->escape_string( $_REQUEST['link'] );
						$attr = $vbulletin->db->escape_string( $_REQUEST['attr'] );
						$order = intval( $_REQUEST['order'] );
						$vbulletin->db->query_write( 'INSERT INTO ' . TABLE_PREFIX . 'fluent (type,name,value,attr,' . TABLE_PREFIX . ( 'fluent.order) VALUES (\'' . $type . '\',\'' . $name . '\',\'' . $link . '\',\'' . $attr . '\',\'' . $order . '\') ' ) );
						print_cp_header( 'تنظیمات پوسته fluent - اضافه کردن لینک' );
						print_cp_redirect( 'fluent.php?do=links' );
						print_cp_footer(  );
					} 
else {
						print_stop_message( 'please_complete_required_fields' );
					}
				}

				print_cp_header( 'تنظیمات پوسته fluent - اضافه کردن لینک' );
				print_form_header( 'fluent', 'add_link' );
				print_table_header( 'اضافه کردن لینک جدید' );
				print_input_row( 'عنوان', 'name', '', true, 35, 250 );
				print_input_row( 'لینک', 'link', '', true, 35, 250 );
				print_input_row( 'ترتیب نمایش', 'order' );
				print_textarea_row( 'attribute ها', 'attr' );
				print_select_row( 'جایگاه', 'mode', array( 2 => 'منوی بالای سایت', 3 => 'دوستان' ), $link['type'] );
				print_submit_row(  );
				echo '<br/><div align="center"><a href="http://hamyar.org">Hamyar</a></div>';
				print_cp_footer(  );
				break;
			}

			case 'upload': {
				if (!empty( $_FILES['logo']['name'] )) {
					if (fluent_findexts( $_FILES['logo']['name'], array( 'png' ) )) {
						unlink( DIR . '/images/fluent/' . $cache['logo']['value'] );
						$name = 'logo.png';
						move_uploaded_file( $_FILES['logo']['tmp_name'], DIR . '/images/fluent/' . $name );
						$vbulletin->db->query( 'UPDATE ' . TABLE_PREFIX . ( 'fluent SET value=\'' . $name . '\' WHERE type=\'1\' AND name=\'logo\' ' ) );
					} 
else {
						$vbphrase['fluent_format'] = '';
						print_cp_message( 'فقط فرمت png برای لوگو قابل فبول هست.', (defined( 'CP_REDIRECT' ) ? CP_REDIRECT : NULL), 1, (defined( 'CP_BACKURL' ) ? CP_BACKURL : NULL), (defined( 'CP_CONTINUE' ) ? true : false) );
					}
				}


				if (!empty( $_FILES['banner1']['name'] )) {
					if (fluent_findexts( $_FILES['banner1']['name'], array( 'png', 'gif', 'jpg', 'jpeg' ) )) {
						unlink( DIR . '/images/fluent/' . $cache['banner1']['value'] );
						$name = time(  ) . 'banner1.' . fluent_findexts( $_FILES['banner1']['name'] );
						move_uploaded_file( $_FILES['banner1']['tmp_name'], DIR . '/images/fluent/' . $name );
						$vbulletin->db->query( 'UPDATE ' . TABLE_PREFIX . ( 'fluent SET value=\'' . $name . '\' WHERE type=\'1\' AND name=\'banner1\' ' ) );
					} 
else {
						$vbphrase['fluent_format'] = '';
						print_cp_message( 'فقط فرمت های jpeg, jpg, png, gif برای بنر تبلیغاتی شماره یک قابل فبول هست.', (defined( 'CP_REDIRECT' ) ? CP_REDIRECT : NULL), 1, (defined( 'CP_BACKURL' ) ? CP_BACKURL : NULL), (defined( 'CP_CONTINUE' ) ? true : false) );
					}
				}


				if (!empty( $_FILES['banner2']['name'] )) {
					if (fluent_findexts( $_FILES['banner2']['name'], array( 'png', 'gif', 'jpg', 'jpeg' ) )) {
						unlink( DIR . '/images/fluent/' . $cache['banner2']['value'] );
						$name = time(  ) . 'banner2.' . fluent_findexts( $_FILES['banner2']['name'] );
						move_uploaded_file( $_FILES['banner2']['tmp_name'], DIR . '/images/fluent/' . $name );
						$vbulletin->db->query( 'UPDATE ' . TABLE_PREFIX . ( 'fluent SET value=\'' . $name . '\' WHERE type=\'1\' AND name=\'banner2\' ' ) );
					} 
else {
						$vbphrase['fluent_format'] = '';
						print_cp_message( 'فقط فرمت های jpeg, jpg, png, gif برای بنر تبلیغاتی شماره دو قابل فبول هست.', (defined( 'CP_REDIRECT' ) ? CP_REDIRECT : NULL), 1, (defined( 'CP_BACKURL' ) ? CP_BACKURL : NULL), (defined( 'CP_CONTINUE' ) ? true : false) );
					}
				}


				if (!empty( $_FILES['banner3']['name'] )) {
					if (fluent_findexts( $_FILES['banner3']['name'], array( 'png', 'gif', 'jpg', 'jpeg' ) )) {
						unlink( DIR . '/images/fluent/' . $cache['banner3']['value'] );
						$name = time(  ) . 'banner3.' . fluent_findexts( $_FILES['banner3']['name'] );
						move_uploaded_file( $_FILES['banner3']['tmp_name'], DIR . '/images/fluent/' . $name );
						$vbulletin->db->query( 'UPDATE ' . TABLE_PREFIX . ( 'fluent SET value=\'' . $name . '\' WHERE type=\'1\' AND name=\'banner3\' ' ) );
					} 
else {
						$vbphrase['fluent_format'] = '';
						print_cp_message( 'فقط فرمت های jpeg, jpg, png, gif برای بنر تبلیغاتی شماره سه قابل فبول هست.', (defined( 'CP_REDIRECT' ) ? CP_REDIRECT : NULL), 1, (defined( 'CP_BACKURL' ) ? CP_BACKURL : NULL), (defined( 'CP_CONTINUE' ) ? true : false) );
					}
				}


				if (!empty( $_FILES['banner4']['name'] )) {
					if (fluent_findexts( $_FILES['banner4']['name'], array( 'png', 'gif', 'jpg', 'jpeg' ) )) {
						unlink( DIR . '/images/fluent/' . $cache['banner4']['value'] );
						$name = time(  ) . 'banner4.' . fluent_findexts( $_FILES['banner4']['name'] );
						move_uploaded_file( $_FILES['banner4']['tmp_name'], DIR . '/images/fluent/' . $name );
						$vbulletin->db->query( 'UPDATE ' . TABLE_PREFIX . ( 'fluent SET value=\'' . $name . '\' WHERE type=\'1\' AND name=\'banner4\' ' ) );
					} 
else {
						$vbphrase['fluent_format'] = '';
						print_cp_message( 'فقط فرمت های jpeg, jpg, png, gif برای بنر تبلیغاتی شماره چهار قابل فبول هست.', (defined( 'CP_REDIRECT' ) ? CP_REDIRECT : NULL), 1, (defined( 'CP_BACKURL' ) ? CP_BACKURL : NULL), (defined( 'CP_CONTINUE' ) ? true : false) );
					}
				}

				print_cp_redirect( 'fluent.php?do=up' );
				break;
			}

			case 'version': {
				print_cp_header( 'تنظیمات پوسته fluent - بررسی کننده ورژن' );
				print_form_header( 'fluent', 'version' );
				print_table_header( 'بررسی کننده ورژن' );
				print_cells_row( array( '<iframe src="http://hamyar.org/download/dl.php?do=vr&plugin=fluent&old_vr=1"  frameborder="0"></iframe>' ), false );
				print_cells_row( array( 'کلیه حقوق برای همیار محفوط است' ), false );
				print_submit_row( 'بررسی دوباره', '' );
				print_cp_footer(  );
				break;
			}

			case 'cache': {
				require_once( DIR . '/includes/functions_databuild.php' );
				vB_Router::setrelativepath( '../' );

				if (!can_administer( 'canadminmaintain' )) {
					print_cp_no_permission(  );
				}

				log_admin_action(  );
				require_once( DIR . '/includes/adminfunctions_template.php' );
				print_cp_header( $vbphrase['maintenance'] );
				build_all_styles( '', '', 'misc.php?' . $vbulletin->session->vars['sessionurl'] . 'do=chooser#style', false, 'standard', false );
				build_all_styles( '', '', 'misc.php?' . $vbulletin->session->vars['sessionurl'] . 'do=chooser#style', false, 'mobile' );
				print_stop_message( 'updated_styles_successfully' );
				print_cp_footer(  );
				break;
			}

			default: {
				exec_header_redirect( 'options.php?do=options&dogroup=fluent' );
				break;
			}
		}

	}

	function fluent_error($error) {
		eval( standard_error( $error ) );
	}

	function fluent_file_write($path, $data, $backup = false) {
		if (file_exists( $path ) != false) {
			if ($backup) {
				$filenamenew = $path . 'old';
				rename( $path, $filenamenew );
			} 
else {
				unlink( $path );
			}
		}


		if ($data != '') {
			$filenum = fopen( $path, 'w' );
			fwrite( $filenum, $data );

			if (fclose( $filenum )) {
				return true;
			}

			return false;
		}

	}

	function fluent_license() {
		return true;
	}

?>